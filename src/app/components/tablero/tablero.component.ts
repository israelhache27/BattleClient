import { Component, OnInit } from '@angular/core';
import Ws from '@adonisjs/websocket-client';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router } from '@angular/router';

//Globals
import { wsUrl } from '../../globals/globals';

//Interface
import { Barco } from '../../interfaces/interfaces';
import { User } from '../../interfaces/interfaces';

@Component({
  selector: 'app-tablero',
  templateUrl: './tablero.component.html',
  styleUrls: ['./tablero.component.css']
})
export class TableroComponent implements OnInit {
  ws = Ws(wsUrl)
  partida
  subscripcion
  Jugador:User = {
    nickname:'',
    player:''
  }
  error:boolean = false
  mensaje:string = ''
  barcos:Array<Barco> = []
  barcosO:Array<Barco> = []
  listo:boolean = false
  ships:number = 6
  Numeros = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]

  constructor() { }

  async ngOnInit() {
    await this.generarBarcos()
    this.recuperaUser()
    this.listo = true;
    this.wsConectar()
    this.wsEscuchar()
  }

  async generarBarcos(){
    let index = 1
    let numero
    let posiciones:Array<number> = []
    for (let i = 0; i < 6; i++) {
      numero = Math.floor(Math.random() * this.Numeros.length)
      let objetoActual:Barco = {id:null, posicion:null, destruido:null, img:"barco4.png"}
      if(this.Numeros.indexOf(numero) != -1){
          let posicion = this.Numeros[numero]
          objetoActual.id = index
          objetoActual.posicion = posicion
          posiciones.push(posicion)
          this.Numeros.splice(this.Numeros.indexOf(posicion),1)
          objetoActual.destruido = false 
          this.barcos.push(objetoActual)
          index++
      }
      else{
        i--
      }
    }
    let posord = posiciones.sort(function(a, b){return a-b})
    let indice = 0
    for(let i = 0; i < 16; i++){
      if(posord[indice] == i){
        let actualbarco = this.barcos.findIndex(barco => barco.posicion == posord[indice])
        this.barcosO[i] = this.barcos[actualbarco]
        indice++
      }
      else{
        let objetoActual:Barco = {id:0, posicion:null, destruido:null, img:"mar.png"}
        objetoActual.posicion = i;
        this.barcosO[i] = objetoActual
      }
    }
  }

  cerrarInfo(){
    this.error = false
  }

  wsConectar(){
    this.ws.connect()
    this.partida = this.ws.subscribe('battle')
    this.subscripcion = this.ws.getSubscription('battle')
  }

  wsDesconectar(){
    this.subscripcion.emit('message:added',{nickname:this.Jugador.nickname,logeo:false})
    this.ws.close()
  }

  wsEscuchar(){
    this.partida.on('message', (data) => {
      if(data.disparo == true && data.player != this.Jugador.player){
        if(this.barcosO[data.posicion].id != 0){
          this.barcosO[data.posicion].destruido = true
          this.barcosO[data.posicion].img = "explosion.gif"
          this.wsAcertado(data.player,data.posicion,true)
          this.ships--
          if(this.ships == 0){
            this.subscripcion.emit('message:added',{noBarcos:true,player:this.Jugador.player})
          }
        }
        else{
          this.barcosO[data.posicion].img = "bandera1.png"
          this.wsAcertado(data.player,data.posicion,false)
        }
      }
      else if(data.noBarcos == true){
        if(data.player == this.Jugador.player){
          this.mensaje = "Perdiste :("
          this.error = true;
        }
        else{
          this.mensaje = "Ganaste :D"
          this.error = true;
        }
      }
      else if(data.acertado == false){
        this.wsAcertado(data.playerñ,data.posicion,false)
      }
    })
  }

  wsAcertado(player,data,destruido){
    this.subscripcion.emit('message:added',{player:player,posicion:data,acertado:destruido})
  }

  recuperaUser(){
    this.Jugador.nickname = Cookie.get('Nickname')
    this.Jugador.player = Cookie.get('Player')
  }
}
