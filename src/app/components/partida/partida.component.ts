import { Component, OnInit } from '@angular/core';
import Ws from '@adonisjs/websocket-client';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router } from '@angular/router';

//Globals
import { wsUrl } from '../../globals/globals';

//Interface
import { Barco } from '../../interfaces/interfaces';
import { User } from '../../interfaces/interfaces';
import { Disparo } from '../../interfaces/interfaces';

@Component({
  selector: 'app-partida',
  templateUrl: './partida.component.html',
  styleUrls: ['./partida.component.css']
})
export class PartidaComponent implements OnInit {
  ws = Ws(wsUrl)
  partida
  subscripcion
  Jugador:User = {
    nickname:'',
    player:''
  }
  error:boolean = false
  mensaje:string = ''
  disparos:Array<Disparo> = []
  turno:boolean = null
  barcos:Array<Barco> = []
  barcosO:Array<Barco> = []
  listo:boolean = false
  ships:number = 6
  declara:boolean = false
  winner:string = ''
  Numeros = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
  audio = new Audio();

  constructor(private router:Router) { }

  ngOnInit() {
    this.audio.src = "../../../assets/img/disparo1.mp3";
    this.audio.load();
    this.generarBarcos()
    this.rellenar()
    this.recuperaUser()
    this.wsConectar()
    this.wsEscuchar()
    if(this.Jugador.player == '1'){
      this.turno = true
    }
    else{
      this.turno = false
    }
  }

  // Metodos de Barco

  async generarBarcos(){
    let index = 1
    let numero
    let posiciones:Array<number> = []
    for (let i = 0; i < 6; i++) {
      numero = Math.floor(Math.random() * this.Numeros.length)
      let objetoActual:Barco = {id:null, posicion:null, destruido:null, img:"barco4.png"}
      if(this.Numeros.indexOf(numero) != -1){
          let posicion = this.Numeros[numero]
          objetoActual.id = index
          objetoActual.posicion = posicion
          posiciones.push(posicion)
          this.Numeros.splice(this.Numeros.indexOf(posicion),1)
          objetoActual.destruido = false 
          this.barcos.push(objetoActual)
          index++
      }
      else{
        i--
      }
    }
    let posord = posiciones.sort(function(a, b){return a-b})
    let indice = 0
    for(let i = 0; i < 16; i++){
      if(posord[indice] == i){
        let actualbarco = this.barcos.findIndex(barco => barco.posicion == posord[indice])
        this.barcosO[i] = this.barcos[actualbarco]
        indice++
      }
      else{
        let objetoActual:Barco = {id:0, posicion:null, destruido:null, img:"mar.png"}
        objetoActual.posicion = i;
        this.barcosO[i] = objetoActual
      }
    }
  }

  // Metodos de Disparo

  rellenar(){
    for (let index = 0; index < 16; index++) {
      let disparo:Disparo = {id:index, posicion:index, disparo:false, img:"mar.png"}
      this.disparos.push(disparo)
    }
    console.log(this.disparos);
  }

  disparar(id){
    if(this.declara == false){
      if(this.turno){
        this.error = false
        if(this.disparos[id].disparo != true){
          this.disparos[id].disparo = true
          this.subscripcion.emit('message:added',{player:this.Jugador.player, posicion:id, disparo:true})
          this.turno = false
        }
        else{
          this.error = true
          this.mensaje = 'Ya has disparado a esta posicion'
        }
      }
      else{
        this.error = true
        this.mensaje = 'No es tu turno'
        this.notifiy()
      }
    }
    else{
      this.error = true
      this.mensaje = 'La Partida Termino'
      this.notifiy()
    }
  }

  // Metodos de User

  recuperaUser(){
    this.Jugador.nickname = Cookie.get('Nickname')
    this.Jugador.player = Cookie.get('Player')
  }

  // Metodos de WebSocket

  wsConectar(){
    this.ws.connect()
    this.partida = this.ws.subscribe('battle')
    this.subscripcion = this.ws.getSubscription('battle')
  }

  wsDesconectar(){
    this.subscripcion.emit('message:added',{nickname:this.Jugador.nickname,logeo:false})
    this.ws.close()
  }

  wsEscuchar(){ 
    this.partida.on('message', (data) => {
      if(data.acertado == true && data.player == this.Jugador.player){
        this.disparos[data.posicion].img = "explosion.gif"
        this.mensaje = "Acertaste un Disparo :3"
        this.notifiy()
      }

      else if(data.acertado == false && data.player == this.Jugador.player){
        this.disparos[data.posicion].img = "bandera1.png"
        this.mensaje = "Fallaste un Disparo :("
        this.notifiy()
      }

      else if(data.disparo == true && data.player != this.Jugador.player){
        var audio = document.getElementById("audio");
        this.audio.play();
        if(this.barcosO[data.posicion].id != 0){
          this.barcosO[data.posicion].destruido = true
          this.barcosO[data.posicion].img = "explosion.gif"
          this.wsAcertado(data.player,data.posicion,true)
          this.ships--
          if(this.ships == 0){
            this.subscripcion.emit('message:added',{ganador:true,player:this.Jugador.player})
          }
        }
        else{
          this.barcosO[data.posicion].img = "bandera1.png"
          this.wsAcertado(data.player,data.posicion,false)
        }
        this.turno = true
        this.mensaje = "Es tu Turno :3"
        this.notifiy()
      }
      else if(data.ganador == true){
        if(data.player != this.Jugador.player){
          this.mensaje = "Ganaste :D"
          this.error = true
          this.turno = false
          this.declara = true
          this.notifiy()
          this.winner = "Ya hay Ganador, el juego no puede continuar: TU GANASTE"
        }
        else{
          this.mensaje = "Perdiste :("
          this.error = true
          this.turno = false
          this.declara = true
          this.notifiy()
          this.winner = "Ya hay Ganador, el juego no puede continuar: TU PERDISTE"
        }
      }
      else if(data.new){
        location.reload()
      }
      else if(data.acertado == false){
        this.wsAcertado(data.playerñ,data.posicion,false)
      }
    })
  }

  wsAcertado(player,data,destruido){
    this.subscripcion.emit('message:added',{player:player,posicion:data,acertado:destruido})
  }

  // Metodos de Alerta

  cerrarInfo(){
    this.error = false
  }

  notifiy() {
    //Navegador
    let options = {
        body: this.mensaje
    };
    let notification = new Notification('Notificacion Jugador ' + this.Jugador.nickname, options);
    notification.onclick = () => {};
  }

  logout(){
    this.subscripcion.emit('message:added',{new:true})
    location.reload()
  }
}
