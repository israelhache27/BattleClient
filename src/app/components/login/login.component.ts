import { Component, OnInit } from '@angular/core';
import Ws from '@adonisjs/websocket-client';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router } from '@angular/router';

//Globals
import { wsUrl } from '../../globals/globals';

//Interface
import { User } from '../../interfaces/interfaces';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  ws = Ws(wsUrl)
  partida
  subscripcion
  error:boolean = false
  cargando:boolean = false
  inicia:boolean = true
  cancela:boolean = false
  conectado:boolean = false
  mensaje:string = ''
  Jugador:User = {
    nickname:'',
    player:''
  }
  constructor(private router:Router) { }

  async ngOnInit() {
    await this.wsConectar()
    this.wsEscuchar()
  }

  async login(){
    if(this.Jugador.nickname.length != 0 && this.Jugador.nickname != "recarga"){
      this.error = false
      this.cargando = true;
      this.cancela = true;
      this.inicia = false;
      this.subscripcion.emit('message:added',{nickname:this.Jugador.nickname,logeo:true})
    }
    else if(this.Jugador.nickname == "recarga"){
      this.Jugador.nickname = ''
      this.error = false
      this.subscripcion.emit('message:added',"Recarga")
    }
    else{
      this.error = true
      this.mensaje = "Ingresa un Nickname para la partida, puta madre"
    }
  }
  
  cerrarInfo(){
    this.error = false
  }

  cancelar(){
    this.cargando = false;
    this.cancela = false;
    this.inicia = true;
  }

  wsConectar(){
    this.ws.connect()
    this.partida = this.ws.subscribe('battle')
    this.subscripcion = this.ws.getSubscription('battle')
  }

  wsDesconectar(){
    this.subscripcion.emit('message:added',{nickname:this.Jugador.nickname,logeo:false})
    this.ws.close()
    this.conectado = false
  }

  wsEscuchar(){
    this.partida.on('message', (data) => {
      console.log(data)
      if(this.Jugador.nickname == data.nickname && data.Player != 2){
        this.Jugador.player = data.Player
        console.log(this.Jugador);
      }
      else if(this.Jugador.nickname == data.nickname && data.Player == 2){
        this.Jugador.player = data.Player
        console.log(this.Jugador);
        this.subscripcion.emit('message:added',"Start")
        this.wsDesconectar()
        Cookie.deleteAll()
        Cookie.set('Nickname',this.Jugador.nickname,null)
        Cookie.set('Player',this.Jugador.player.toString(),null)
        this.router.navigateByUrl('/juego')
      }
      else if(data == 'Full'){
        this.error = true
        this.mensaje = "Lo sentimos, de momento, el servidor esta ejecutando un combate, porfavor regresa luego :("
        this.cancelar()
      }
      else if(data == 'Start'){
        this.wsDesconectar()
        Cookie.deleteAll()
        Cookie.set('Nickname',this.Jugador.nickname,null)
        Cookie.set('Player',this.Jugador.player.toString(),null)
        this.router.navigateByUrl('/juego')
      }
    })
  }
}
