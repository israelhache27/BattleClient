import { Component, OnInit } from '@angular/core';
import Ws from '@adonisjs/websocket-client';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router } from '@angular/router';

//Globals
import { wsUrl } from '../../globals/globals';

//Interface
import { User } from '../../interfaces/interfaces';

@Component({
  selector: 'app-juego',
  templateUrl: './juego.component.html',
  styleUrls: ['./juego.component.css']
})
export class JuegoComponent implements OnInit {
  ws = Ws(wsUrl)
  partida
  subscripcion
  error:boolean = false
  Jugador:User = {
    nickname:'',
    player:''
  }
  turno:boolean = null
  constructor() { }

  ngOnInit() {
    this.recuperaUser()
    this.wsConectar()
    this.wsEscuchar()
  }

  cerrarInfo(){
    this.error = false
  }

  wsConectar(){
    this.ws.connect()
    this.partida = this.ws.subscribe('battle')
    this.subscripcion = this.ws.getSubscription('battle')
  }

  wsDesconectar(){
    this.subscripcion.emit('message:added',{nickname:this.Jugador.nickname,logeo:false})
    this.ws.close()
  }

  wsEscuchar(){
    this.partida.on('message', (data) => {
      console.log(data)
      if(data.noBarcos){
        this.turno = false
        this.subscripcion.emit('message:added',{player:this.Jugador.player,turno:false})
      }
    })
  }

  recuperaUser(){
    this.Jugador.nickname = Cookie.get('Nickname')
    this.Jugador.player = Cookie.get('Player')
  }
}
