import { Component, OnInit } from '@angular/core';
import Ws from '@adonisjs/websocket-client';
import { Cookie } from 'ng2-cookies/ng2-cookies';

//Globals
import { wsUrl } from '../../globals/globals';

//Interface
import { Disparo } from '../../interfaces/interfaces';
import { User } from '../../interfaces/interfaces';

@Component({
  selector: 'app-disparo',
  templateUrl: './disparo.component.html',
  styleUrls: ['./disparo.component.css']
})
export class DisparoComponent implements OnInit {
  ws = Ws(wsUrl)
  partida
  subscripcion
  Jugador:User = {
    nickname:'',
    player:''
  }
  error:boolean = false
  mensaje:string = ''
  disparos:Array<Disparo> = []
  turno:boolean = null
  constructor() { }

  ngOnInit() {
    this.rellenar()
    this.recuperaUser()
    this.wsConectar()
    this.wsEscuchar()
    if(this.Jugador.player == '1'){
      this.turno = true
    }
    else{
      this.turno = false
    }
  }

  rellenar(){
    for (let index = 0; index < 16; index++) {
      let disparo:Disparo = {id:index, posicion:index, disparo:false, img:"mar.png"}
      this.disparos.push(disparo)
    }
    console.log(this.disparos);
  }

  disparar(id){
    if(this.turno){
      this.error = false
      if(this.disparos[id].disparo != true){
        this.disparos[id].disparo = true
        this.subscripcion.emit('message:added',{player:this.Jugador.player, posicion:id, disparo:true})
        this.turno = false
      }
      else{
        this.error = true
        this.mensaje = 'Ya has disparado a esta posicion'
      }
    }
    else{
      this.error = true
      this.mensaje = 'No es tu turno'
    }
    
  }

  cerrarInfo(){
    this.error = false
  }

  wsConectar(){
    this.ws.connect()
    this.partida = this.ws.subscribe('battle')
    this.subscripcion = this.ws.getSubscription('battle')
  }

  wsDesconectar(){
    this.subscripcion.emit('message:added',{nickname:this.Jugador.nickname,logeo:false})
    this.ws.close()
  }

  wsEscuchar(){
    this.partida.on('message', (data) => {
      console.log(data)
      if(data.acertado == true && data.player == this.Jugador.player){
        this.disparos[data.posicion].img = "explosion.gif"
      }
      else if(data.acertado == false && data.player == this.Jugador.player){
        this.disparos[data.posicion].img = "bandera1.png"
      }
      else if(data.disparo == true && data.player != this.Jugador.player){
        this.turno = true
      }
    })
  }

  recuperaUser(){
    this.Jugador.nickname = Cookie.get('Nickname')
    this.Jugador.player = Cookie.get('Player')
  }

}
