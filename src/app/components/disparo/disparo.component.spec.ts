import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisparoComponent } from './disparo.component';

describe('DisparoComponent', () => {
  let component: DisparoComponent;
  let fixture: ComponentFixture<DisparoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisparoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisparoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
