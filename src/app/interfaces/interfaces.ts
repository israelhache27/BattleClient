// export interface Profesor{
//     nombre: string;
//     apellido: string;
//     matricula: string;
// }

export interface Barco{
    id:number;
    posicion:number;
    destruido:boolean;
    img:string;
}

export interface User{
    nickname:string;
    player:string;
}

export interface Disparo{
    id:number;
    posicion:number;
    disparo:boolean;
    img:string;
}