import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';


//Guards

//Componentes
import { AppComponent } from '../components/appcomponent/app.component';
import { LoginComponent } from '../components/login/login.component';
import { TableroComponent } from '../components/tablero/tablero.component';
import { DisparoComponent } from '../components/disparo/disparo.component';
import { JuegoComponent } from '../components/juego/juego.component';
import { PartidaComponent } from '../components/partida/partida.component';




const appRoutes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'juego', component: PartidaComponent},
  { path: '**', redirectTo: 'login'},
  { path: '', redirectTo: 'login', pathMatch: 'full'},
];

export const Components = [
  AppComponent,
  LoginComponent,
  JuegoComponent,
  DisparoComponent,
  TableroComponent,
  PartidaComponent
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes,{enableTracing: true,}),
  ],
  exports: [
    RouterModule,
  ],
  declarations: [
    
  ],
  providers: [
    
  ]
})
export class AppRoutingModule { }
