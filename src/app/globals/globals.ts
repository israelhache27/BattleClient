/**
 * Url del servidor
 */
export const serverUrluser: string = 'http://localhost:3333/';
export const serverUrljuego: string = 'http://localhost:3333/';


/**
 * Url del servidor del WebSocket
 */
export const wsUrl: string = 'ws://127.0.0.1:3333';

/**
 * Nombre de la cookie
 */
export const TokenJWT: string = 'JWT-Token';